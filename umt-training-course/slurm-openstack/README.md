
### Slurm & Openstack

####  tutorial_slurm_1_ssh_access.pdf
```
First tutorial:
  Need a GenOuest account
  Explain how to configure SSH Access to GenOuest on Windows (with PuTTY)
```

####  tutorial_slurm_2_data_cluster.pdf
```
Second tutorial:
  Need a SSH Access (see Tutorial #1)
  Explain how to access to cluster (with Bash) and store some data
```

####  tutorial_slurm...: not finished
```
Third tutorial:
  Need a SSH Access (see Tutorial #1)
  Cloud and VM (openstack.genouest.org)
```
